input = document.getElementById("textToCopy");
button = document.getElementById("copyButton");
alertBox = document.getElementById("alertBox");

button.addEventListener("click", () => {
  let alertFail = document.createElement("div");
  alertFail.classList.add("alert", "alert-fail");
  alertFail.innerHTML = "Cannot copy null content !";

  let alertSuccess = document.createElement("div");
  alertSuccess.classList.add("alert", "alert-success");
  alertSuccess.innerHTML = "Content copied to clipboard !";

  if (input.value == "") {
    alertBox.appendChild(alertFail);
    fadeOutAlert(alertFail);
    removeChildFromParentAlertBox(alertBox, alertFail);
  } else {
    navigator.clipboard.writeText(input.value);
    alertBox.appendChild(alertSuccess);
    fadeOutAlert(alertSuccess);
    removeChildFromParentAlertBox(alertBox, alertSuccess);
  }
});

function fadeOutAlert(alert) {
  setTimeout(() => {
    alert.style.opacity = "0";
    alert.style.transition = "0.5s";
    alert.style.transform = "translateX(100%)";
  }, 4000);
}

function removeChildFromParentAlertBox(alertBox, alert) {
  setTimeout(() => {
    alertBox.removeChild(alert);
  }, 8000);
}
